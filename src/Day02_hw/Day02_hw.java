package Day02_hw;

// question 1: calculate the parameter and the area are the same actions in all different shapes: square, circle....
// we use abstract class because we don't need to repeat writing these parts.
// if we want to add more shapes, we don't need to touch the "shape" class at all. Simply create a new subclass for new shape.

// method with abstract
abstract class Shape{
	
	public abstract double getArea(double length);
	public abstract double getPerimeter(double length);
}
class Square extends Shape{
	double length;
	public Square(double length) {
		this.length = length;
	}
	@Override
	public double getArea(double length) {
		return Math.pow(length, 2);
	}

	@Override
	public double getPerimeter(double length) {
		return length*4;
	}
	
}
class Circle extends Shape{
	double radius;
	final double pi = Math.PI;
	public Circle(double radius) {
		this.radius = radius;
	}
	@Override
	public double getArea(double radius) {
		return (pi * Math.pow(radius, 2));
	}

	@Override
	public double getPerimeter(double radius) {
		return (2 * pi * radius);
	}
	
}

// method without abstract

class SquareMethod2 {
	double length;
	public SquareMethod2 (double length) {
		this.length = length;
	}
	public double getArea(double length) {
		return Math.pow(length, 2);
	}

	public double getPerimeter(double length) {
		return length*4;
	}
	
}
class CircleMethod2{
	double radius;
	final double pi = Math.PI;
	public CircleMethod2(double radius) {
		this.radius = radius;
	}
	public double getArea(double radius) {
		return (pi * Math.pow(radius, 2));
	}
	public double getPerimeter(double radius) {
		return (2 * pi * radius);
	}
	
}


// question 2: animal

abstract class Animal{
	abstract void sound();
}

class Cats extends Animal{
	public void sound() {
		System.out.println("miao~~");
	}
}
class Dogs extends Animal{
	public void sound() {
		System.out.println("bow~~~");
	}
}

// question 3: percentage

abstract class Marks {
    abstract Double getPercentage();
}

class A extends Marks {
    private Double sub1, sub2, sub3;
    A (Double sub1, Double sub2, Double sub3) {
        this.sub1 = sub1;
        this.sub2 = sub2;
        this.sub3 = sub3;
    }
    public Double getPercentage() {
        Double percentage = (sub1 + sub2 + sub3) / 3;
        return percentage;
    }
}

class B extends Marks {
    private Double sub1, sub2, sub3, sub4;
    B (Double sub1, Double sub2, Double sub3, Double sub4) {
        this.sub1 = sub1;
        this.sub2 = sub2;
        this.sub3 = sub3;
        this.sub4 = sub4;
    }
    public Double getPercentage() {
        Double percentage = (sub1 + sub2 + sub3 + sub4) / 4;
        return percentage;
    }
}

public class Day02_hw {

	public static void main(String[] args) {
		//abstract square test
		double length = 5;
		Shape square = new Square(length);
		System.out.println("Square Area: " + square.getArea(length) +"\nSquare Perimeter: " + square.getPerimeter(length));
		//abstract circle test
		double radius = 3;
		Shape circle = new Circle(radius);
		System.out.println("Circle Area: " + circle.getArea(radius) +"\nCircle Perimeter: " + circle.getPerimeter(radius));
		System.out.println();

		//no-abstract square test
		double lengthMethod2 = 5;
		SquareMethod2 squareMethod2 = new SquareMethod2(lengthMethod2);
		System.out.println("Square without abstract method Area: " + squareMethod2.getArea(length) +"\nSquare without abstract method Perimeter: " + squareMethod2.getPerimeter(length));
		//no-abstract circle test
		double radiusMethod2 = 3;
		CircleMethod2 circleMethod2= new CircleMethod2(radiusMethod2);
		System.out.println("Circle without abstract method Area: " + circleMethod2.getArea(radius) +"\nCircle without abstract method Perimeter: " + circleMethod2.getPerimeter(radius));
		System.out.println();
		
		// test question 2: animal
		Animal cat = new Cats();
		cat.sound();
		Animal dog = new Dogs();
		dog.sound();
		System.out.println();

		// test question 3: percentage
        A a = new A (50.0, 60.0, 70.0);
        B b = new B (50.0, 60.0, 70.0, 80.0);
        System.out.println("percentage of student A is " + a.getPercentage() + "%");
        System.out.println("percentage of student B is " + b.getPercentage() + "%");	
	}

}
